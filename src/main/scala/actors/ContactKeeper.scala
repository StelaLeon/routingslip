package actors

import akka.actor.Actor
import com.example.RegisterCustomer

class ContactKeeper extends Actor{

  def receive = {
    case registerCustomer : RegisterCustomer =>
      val contactInfo = registerCustomer.registrationData.contactInformation

      println(s"ContactKeeper: handling register customer to keep contact information: $contactInfo")

      registerCustomer.advance()

      context.stop(self)

    case message: Any =>
      print(s"ContactKeeper: unexpected message: $message")
  }
}
