package actors

import akka.actor.Actor
import com.example.RegisterCustomer

class CustomerVault extends Actor {

  def receive = {
    case registerCustomer: RegisterCustomer =>

      val customerInformation = registerCustomer.registrationData.customerInformation

      println(s"CustomerVault: handling register customer to create a new customer: $customerInformation")

      registerCustomer.advance()
      context.stop(self)

    case message:Any=>
      println(s"CustomerVault: unexpected message $message")
  }
}
