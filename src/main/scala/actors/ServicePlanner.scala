package actors

import akka.actor.Actor
import akka.actor.Actor.Receive
import com.example.RegisterCustomer


class ServicePlanner extends Actor{

  def receive = {
    case registerCustomer: RegisterCustomer =>
      val serviceOption = registerCustomer.registrationData.serviceOption

      println(s"ServicePlanner: handling register customer to plan a new customer service: $serviceOption")

      registerCustomer.advance()

      context.stop(self)

    case message: Any =>
      println(s"ServicePlanner: unexpected $message")
  }
}
