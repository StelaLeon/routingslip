package actors

import akka.actor.Actor
import com.example.RegisterCustomer

class CreditChecker extends Actor{

  def receive = {
    case registerCustomer: RegisterCustomer =>
      val federalTaxId = registerCustomer.registrationData
        .customerInformation.federalTaxId

      println(s"CreditChecker: handling register customer to perform credit check: $federalTaxId")

      registerCustomer.advance()

      context.stop(self)

    case message: Any =>
      println(s"CreditChecker: unexpected: $message")
  }

}
