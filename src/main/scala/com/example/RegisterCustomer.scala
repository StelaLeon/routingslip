package com.example

import main.scala.com.example.{RegistrationData, RegistrationProcess}

case class RegisterCustomer(
                      val registrationData: RegistrationData,
                      val registrationProcess: RegistrationProcess
                      ) {

  def advance(): Unit ={
    val advancedProcess = registrationProcess.stepCompleted

    if(! advancedProcess.isCompleted){
      advancedProcess.nextStep().processor ! RegisterCustomer(registrationData, advancedProcess)
    }
  }
}
