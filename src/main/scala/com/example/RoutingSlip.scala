package com.example

import akka.actor.ActorSystem
import com.typesafe.config.{Config, ConfigFactory}
import main.scala.com.example._

import scala.util.Random

object RoutingSlip extends App {
  val system = ActorSystem("MyActorSystem")

  val processId = java.util.UUID.randomUUID().toString

  val step1 = ProcessStep("create_customer",
    ServiceRegistry.customerVault(system,processId))

  val step2 = ProcessStep("set_up_contact_info",
    ServiceRegistry.contactKeeper(system,processId))

  val step3 = ProcessStep("serlect_service_plan",
    ServiceRegistry.servicePlanner(system,processId))

  val step4 = ProcessStep("check_credit",
    ServiceRegistry.creditChecker(system, processId))



  val registrationProcess = new RegistrationProcess(processId,Vector(step1, step2, step3, step4))

  val registrationData = new RegistrationData(
    CustomerInformation(
      "ABC, GMBH.", "123-45-2345"),
    ContactInformation(PostalAddress(
      "Main Strasse 32, ","suite 10", "Berlin","Berlin","12053"),
      Telephone("")),
    ServiceOption("88-21921","A description"))

  val registerCustomer = RegisterCustomer(registrationData,registrationProcess)

  registrationProcess.nextStep().processor ! registerCustomer

//  Await.ready(system.whenTerminated, 10 seconds)
}